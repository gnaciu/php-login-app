<?php

function usernameExists($username, $PDO) {

    try{

        $stmt = $PDO->prepare("SELECT username from user where username = :name");
        $stmt->bindParam(":name", $username);
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
        } catch (PDOException $e) {
            return NULL;
        } 
}

?>
