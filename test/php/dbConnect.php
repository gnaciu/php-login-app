<?php

$dsn = 'mysql:dbname=FairHire;host=localhost';

try {
    $PDO = new PDO($dsn, 'root', '');
    $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}  catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    return var_dump(http_response_code($SERVICE_UNAVAILABLE));
}

?>