

 function errorMessage(message){
    $("#notification .alert-info").remove()
    $("#notification .alert-danger").remove()

    $('#notification ').append('<div class="alert alert-danger"> <strong>Oh snap! </strong>' + message + '</div>');
}

$("#login_form").submit(function(e) {
    e.preventDefault();

var message = ""

let username =  $("#login_username").val()
let pass =  $("#login_pass").val()

if (!check_username(username)){
    message += "The entered username is not valid! It should be 6 - 20 characters long & alphanumeric!\n"
};
 if(!check_pass(pass)){
    message += "The entered password is not valid! It should be 10 - 25 characters long & also alphanumeric!\n"
 }


if (message === ""){
    let user = {username: username, password: pass}

    $.post("php/login.php", user, function(data){
        user.password =  CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(user.password));
        localStorage.setItem("user", JSON.stringify(user));

        $("#notification .alert-info").remove()
        $("#notification .alert-danger").remove()
        $('#notification ').append('<div class="alert alert-success"> <h3>Login successfull!</h3> </div>');
        setTimeout(function (){
            window.location.replace("http://localhost/test/routes/profile.html");
          }, 1000);
        


      }).always(function(xhr, textStatus){
        if(textStatus === "error"){
          switch(xhr.status){
              case 400:
                errorMessage("Username must be 6 - 20 characters long  & alphanumeric.\n"
                  + "Passwords must be 10 - 25 characters long  & alphanumeric.");
                  break;
              case 401:
                errorMessage("Incorrect user credentials.");
                  break;
              case 503:
                errorMessage("Database is unavailable.");
                  break;
              default:
                errorMessage("Unknown server error.")
          }
        }
    })
} else {
    errorMessage(message);
}
});