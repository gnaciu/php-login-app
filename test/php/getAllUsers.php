<?php

require_once('statusCodes.php');
require_once('dbConnect.php');
include 'existingUser.php';
include 'validation.php';

$username = $_POST['username'];
$password = $_POST['password'];

if (empty($_POST) || !validCredentials($username, $password)){

    return header( "Bad Request", true, $BAD_REQUEST);   
 }
$userExists = usernameExists($username, $PDO);

if(is_null($userExists)){

    $PDO = null;
    return header( "Server error", true, $INTERNAL_SERVER_ERROR);

} else if ($userExists === false) {
    
    $PDO = null;
    return header( "Unauthorized login attempt", true, $UNAUTHORIZED);

} else{

try{

    $stmt = $PDO->prepare("SELECT password FROM user where username = :name");
    $stmt->bindParam(":name", $username);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if ( password_verify($password, $row['password']) === true ){  

        $sql =  "SELECT username, lastLogged FROM user ORDER BY lastLogged DESC";
        $users = $PDO->query($sql)->fetchAll(PDO::FETCH_KEY_PAIR);


        $PDO = null;
        header( "OK", true, $OK);
        echo json_encode($users);


    } else {

        $PDO = null;
        return header( "Unauthorized login attempt", true, $UNAUTHORIZED);
    }

    } catch (PDOException $e) {

    $PDO = null;
    return header( "Server error", true, $INTERNAL_SERVER_ERROR);
    }
}

?>

