 function check_pass2(pass1, pass2) {
    if( pass1 === pass2 ){
        return true;
    } else {
        return false;
    }
 }

 function errorMessage(message){
    $("#notification .alert-info").remove()
    $("#notification .alert-danger").remove()

    $('#notification ').append('<div class="alert alert-danger"> <strong>Oh snap! </strong>' + message + '</div>');
}
 

$("#registration_form").submit(function(e) {
    e.preventDefault();

let username =  $("#form_username").val()
let pass1 =  $("#form_pass1").val()
let pass2 =  $("#form_pass2").val()

var message = "";

if (!check_username(username)){
    message += "The entered username is not valid! It should be 6 - 20 characters long & alphanumeric!\n"
};
 if(!check_pass(pass1)){
    message += "The entered password is not valid! It should be 10 - 25 characters long & alphanumeric!\n"
 }
 if(!check_pass2(pass1, pass2)){
    message += "Make sure your passwords mach!"
 }

if ( message === "" ){
    $.post("../php/register.php", {username: username, password: pass1}, ()=>{

      }).done(function(){
        $("#notification .alert-info").remove()
        $("#notification .alert-danger").remove()
        $('#notification ').append('<div class="alert alert-success"> <h3>Registration successfull!</h3></div>');
        setTimeout(function (){
            window.location.replace("http://localhost/test/index.html")
          }, 1000);
        
      })
      .always(function(xhr, textStatus){

          if(textStatus === "error"){
            switch(xhr.status){
                case 400:
                    errorMessage("Username must be 6 - 20 characters long  & alphanumeric.\n"
                    + "Passwords must be 10 - 25 characters long  & alphanumeric.");
                    break;
                case 409:
                    errorMessage("User already exists! Choose a different username.");
                    break;
                case 503:
                    errorMessage("Database is unavailable.");
                    break;
                default:
                    errorMessage("Unknown server error.")
            }
          }
      }
      )
      
} else {
    errorMessage(message);
    }
});