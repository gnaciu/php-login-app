<?php

require_once('statusCodes.php');
require_once('dbConnect.php');
include 'existingUser.php';
include 'validation.php';

$username = $_POST['username'];
$password = $_POST['password'];

if (empty($_POST) || !validCredentials($username, $password)){

    return header( "Bad Request", true, $BAD_REQUEST);   
 }

$userExists = usernameExists($username, $PDO);

if(is_null($userExists)){

    $PDO = null;
    return header( "Server error", true, $INTERNAL_SERVER_ERROR);

} else if ($userExists === true) {
    $PDO = null;
    return header( "User alredy exists", true, $CONFLICT);
}

try{
    $timestamp = date('Y-m-d H:i:s');

    $stmt = $PDO->prepare('INSERT INTO user (username, password, lastLogged ) VALUES (:username, :password, :timestamp)');
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':password', password_hash($password,  PASSWORD_DEFAULT));
    $stmt->bindParam(':timestamp', $timestamp);

    $stmt->execute();

    } catch (PDOException $e) {
        
        $PDO = null;
        echo 'Insertion failed: ' . $e->getMessage();
        return header( "Server error", true, $INTERNAL_SERVER_ERROR);
    }
$PDO = null;
return header( "OK", true, $OK);

?>

