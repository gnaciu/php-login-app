const logOut = function(){
    localStorage.clear();
    window.location.replace("http://localhost/test/index.html")
}

let user = JSON.parse(localStorage.getItem('user'));

if (user && user.password){
    try{
        user.password = CryptoJS.enc.Base64.parse(user.password).toString(CryptoJS.enc.Utf8);

    } catch (e){
        logOut()
    }
} else{
    logOut();
}

$.post("../php/login.php", user, function(data){
    $("#greet").text("Welcome back " + user.username +"!");
    $.post("../php/getAllUsers.php", user, function(data){

        let users = JSON.parse(data)
        $('#users .list-group li').remove();

        for (var key of Object.keys(users)) {
        
    $('#users .list-group').append('<li class="list-group-item d-flex justify-content-between align-items-center">' + key
     + '<span class="badge badge-primary badge-pill"> ' +users[key] + '</span> </li>')
        }
    })


  }).always(function(xhr, textStatus){
    if(textStatus === "error"){
        logOut();
    }});

