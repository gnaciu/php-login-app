<?php

require_once('statusCodes.php');
require_once('dbConnect.php');
include 'existingUser.php';
include 'validation.php';

$username = $_POST['username'];
$password = $_POST['password'];

if (empty($_POST) || !validCredentials($username, $password)){

    return header( "Bad Request", true, $BAD_REQUEST);   
 }
$userExists = usernameExists($username, $PDO);

if(is_null($userExists)){

    $PDO = null;
    return header( "Server error", true, $INTERNAL_SERVER_ERROR);

} else if ($userExists === false) {
    
    $PDO = null;
    return header( "Unauthorized login attempt", true, $UNAUTHORIZED);

} else{

try{

    $timestamp = date('Y-m-d H:i:s');

    $stmt = $PDO->prepare("SELECT password from user where username = :name");
    $stmt->bindParam(":name", $username);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if ( password_verify($password, $row['password']) === true ){  

        $stmt = $PDO->prepare("UPDATE user SET lastLogged = :timestamp where username = :name");
        $stmt->bindParam(":name", $username);
        $stmt->bindParam(':timestamp', $timestamp);
        $stmt->execute();

        $PDO = null;
        return header( "OK", true, $OK);
        
    } else {

        $PDO = null;
        return header( "Unauthorized login attempt", true, $UNAUTHORIZED);
    }

    } catch (PDOException $e) {

    $PDO = null;
    return header( "Server error", true, $INTERNAL_SERVER_ERROR);
    }
}

?>

