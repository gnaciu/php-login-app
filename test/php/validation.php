<?php
function validCredentials($username, $password){

if (isset($username) === false || empty($username) === true 
    || isset($password) === false || empty($password) === true) {

    return false;
}

if( gettype($username) !== "string" || gettype($password) !== "string" ){

    return false;
}


if ( !ctype_alnum($username)  || !ctype_alnum($password) 
    || strlen($username) > 20 || strlen($username) <= 5 || strlen($password) > 25 || strlen($password) <= 9 ){

    return false;
    }

return true;
}
?>