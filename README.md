This is a simple application written in jQuery and PHP for server logic.

All the functionality is to create new users and log in to see a list of last user log-ins.
It also has password encrytion on both server and client.

Screenshots:

![log in screen](logger.png)

![registration form](registration.png)

![profile page](profile.png)

